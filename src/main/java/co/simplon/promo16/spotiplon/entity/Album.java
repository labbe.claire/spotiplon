package co.simplon.promo16.spotiplon.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Album {
    @Id
    @GeneratedValue (strategy=GenerationType.IDENTITY)
    private Integer id;
    
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate released;
    private String cover;

    @ManyToOne
    private Artist artist;

    @ManyToMany(mappedBy = "albums")
    private List<Song> songs = new ArrayList<>();
    
    public Album() {
    }
    public Album(String name, LocalDate released, String cover) {
        this.name = name;
        this.released = released;
        this.cover = cover;
    }
    public Album(Integer id, String name, LocalDate released, String cover) {
        this.id = id;
        this.name = name;
        this.released = released;
        this.cover = cover;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public LocalDate getReleased() {
        return released;
    }
    public void setReleased(LocalDate released) {
        this.released = released;
    }
    public String getCover() {
        return cover;
    }
    public void setCover(String cover) {
        this.cover = cover;
    }
    public Artist getArtist() {
        return artist;
    }
    public void setArtist(Artist artist) {
        this.artist = artist;
    }
    public List<Song> getSongs() {
        return songs;
    }
    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
    @Override
    public String toString() {
        return "Album [artist=" + artist + ", cover=" + cover + ", id=" + id + ", name=" + name + ", released="
                + released + ", songs=" + songs + "]";
    }

    public int getDuration() {
        int duration = 0;
        for (Song song : songs) {
            duration+=song.getDuration();
        }
        return duration;
    }


}
