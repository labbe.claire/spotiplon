package co.simplon.promo16.spotiplon.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.spotiplon.entity.Artist;

@Repository
@Transactional
public class ArtistRepositoryWithEM {

    @Autowired
    private EntityManager em;

    public List<Artist> findAll() {
        return em.createQuery("from Artist", Artist.class).getResultList();
    }

    public Artist findById(Integer id) {
        return em.find(Artist.class, id);
    }

    public void addArtist(Artist artist) {
        em.persist(artist);
    }

    public void updateArtist(Artist artist) {
        em.refresh(artist);
    }

    public void deleteArtist(Artist artist) {
        em.remove(artist);
    }
}
