package co.simplon.promo16.spotiplon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.promo16.spotiplon.entity.Artist;

public interface ArtistRepository extends JpaRepository<Artist,Integer> {
    List<Artist> findByName(String name);
    List<Artist> findByNameContains(String name);
}
