package co.simplon.promo16.spotiplon.controller;

import java.io.IOException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo16.spotiplon.entity.Album;
import co.simplon.promo16.spotiplon.repository.AlbumRepository;
import co.simplon.promo16.spotiplon.repository.ArtistRepository;
import co.simplon.promo16.spotiplon.service.Uploader;

@Controller
public class HomeController {
    @Autowired
    private AlbumRepository repo;

    @Autowired
    private ArtistRepository arepo;

    @Autowired
    private Uploader uploader;

    @GetMapping("/")
    public String showHomePage(Model model) {

        model.addAttribute("albums", repo.findAll());
        return "index";
    }

    @GetMapping("/album/{id}")
    public String showOneAlbum(@PathVariable int id, Model model) {
        Album album = repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        model.addAttribute("album", album);
        return "album";
    }

    @GetMapping("/add-album")
    public String createAlbum(Model model) {
        model.addAttribute("album", new Album());
        model.addAttribute("artists", arepo.findAll());
        return "/add-album";
    }

    @PostMapping("/add-album")
    public String processForm(Album album, @RequestParam("file") MultipartFile file) {
        try {
            album.setCover(uploader.upload(file));
            repo.save(album);
        } catch (IOException e) {
            
            e.printStackTrace();
        }
        return "redirect:/";
    }


}
