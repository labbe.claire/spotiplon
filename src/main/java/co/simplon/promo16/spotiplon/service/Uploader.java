package co.simplon.promo16.spotiplon.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class Uploader {

    public String upload(MultipartFile file) throws IOException {
        String fileName = "/uploads/" + UUID.randomUUID() + "."
                + StringUtils.getFilenameExtension(file.getOriginalFilename());

        Path path = Paths.get("src/main/resources/static" + fileName);

        Files.copy(file.getInputStream(), path);

        return fileName;

    }
}
